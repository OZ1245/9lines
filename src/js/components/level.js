// import './vendor';

const level = {
	$level: $('.j-level'),
	$meterArrow: $('.j-meter-arrow'),
	$list: $('.j-skill-check'),
	listCount: $('.j-skill-check').length,
	selectedCount: 0,
	minLevel: 0,
	maxLevel: 1000,
	currentLevel: parseInt($('.j-level').text(), 2),
	minArrowLevel: -90,
	maxArrowLevel: 90,
	centerScore: 500,
	currentArrowLevel: -90,
	getLevel() {
		this.selectedCount = this.$list.filter(':checked').length;

		return Math.round(this.maxLevel * this.selectedCount / this.listCount);
	},
	renderLevel(mode = 'increment') {
		let i = this.currentLevel;
		const num = this.getLevel();
		const step = Math.round(1000 * 1 / num);

		if (mode === 'increment') {
			const int = setInterval(() => {
				if (i <= num) {
					this.currentLevel = Math.round(i);
					this.$level.html(this.currentLevel);
					this.getArrowLevel();
					this.renderArrowLevel();
				} else {
					clearInterval(int);
				}
				i++;
			}, step);
		} else if (mode === 'decrement') {
			const int = setInterval(() => {
				if (i >= num) {
					this.currentLevel = Math.round(i);
					this.$level.html(this.currentLevel);
					this.getArrowLevel();
					this.renderArrowLevel();
				} else {
					clearInterval(int);
				}
				i--;
			}, step);
		}
	},
	getArrowLevel() {
		if (this.currentLevel < this.centerScore) {
			const temp = Math.round(this.currentLevel * this.minArrowLevel / this.centerScore);
			this.currentArrowLevel = this.minArrowLevel - temp;
		} else if (this.currentLevel === this.centerScore) {
			this.currentArrowLevel = 0;
		} else {
			const temp = Math.round(this.currentLevel * this.maxArrowLevel / this.centerScore);
			this.currentArrowLevel = temp - this.maxArrowLevel;
		}
	},
	renderArrowLevel() {
		this.$meterArrow.css('transform', `rotate(${this.currentArrowLevel}deg)`);
	},
	checkSkills() {
		this.selectedCount = this.$list.filter(':checked').length;
		if (this.selectedCount > 0) {
			this.renderLevel('increment');
		}
	},
	init() {
		const $this = this;

		$this.checkSkills();

		$this.$list.on('click', function () {
			if ($(this).prop('checked') === true) {
				$this.renderLevel('increment');
			} else {
				$this.renderLevel('decrement');
			}
		});
	},
};

export {level};
