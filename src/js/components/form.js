const form = {
	$fields: $('.j-field'),
	focusField($field) {
		this.$fields.each(function () {
			$(this).removeClass('_active');
			if ($(this).find('input').val() !== '') {
				$(this).addClass('_filled');
			}
		});
		$field
			.addClass('_active')
			.find('input')
			.trigger('focus');
	},
	init() {
		const $this = this;

		$this.$fields.on('click', function () {
			$this.focusField($(this));
		});
	},
};

export {form};
